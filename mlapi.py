from fastapi import FastAPI
from pydantic import BaseModel
import pickle
import pandas

app=FastAPI()

class SpamItem(BaseModel):
    Message:str

with open("spam_pred_model.pkl","rb") as f:
    model=pickle.load(f)

with open("countvectorizer.pkl","rb") as f:
    cv=pickle.load(f)
@app.get("/")
async def root():
    return {"hello":"world"}

@app.post("/predict")
def spam_endpoint(item:SpamItem):
    data=item.dict()
    message=data['Message']
    print(message)
    message_count=cv.transform([message])
    predicition=model.predict(message_count)
    print(predicition)
    if predicition[0]==1:
        pred="spam"
    else:
        pred="ham"

    return { "Prediction":pred}
